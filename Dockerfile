FROM tomcat:8
COPY target/*.war /usr/local/tomcat/webapps/student.war
RUN sed -i -e '/org.apache.catalina.webresources/a JAVA_OPTS="-DDBENDPOINT=$DBENDPOINT -DDBNAME=$DBNAME -DDBUSER=$DBUSER -DDBPASS=$DBPASS"' /usr/local/tomcat/bin/catalina.sh
ADD context.xml /usr/local/tomcat/conf/context.xml
